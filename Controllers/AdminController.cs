﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Platform.Models;
using Platform.Models.ViewModel;
using Platform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Platform.Controllers
{
    public class AdminController : Controller
    {
        private readonly IAdminRepository _superadmin;
        public AdminController(IAdminRepository superadmin)
        {
            _superadmin = superadmin;
        }
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Test()
        {
            return View();
        }
        public ActionResult AllUsers()
        {
            var users = _superadmin.GetAllUsers();
            return View(users);
        }
        public ActionResult AllRoles()
        {
            var roles = _superadmin.GetAllRoles();
            return View(roles);
        }
        [HttpGet]
        public ActionResult CreateRole()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateRole(IdentityRole role)
        {
            if (ModelState.IsValid)
            {
                var result = _superadmin.RoleExistsAsync(role.Name);
                if (result.Result == true)
                {
                    ViewBag.Message = "Role already exist";
                    return View("CreateRole", "Admin");
                }
                _superadmin.CreateRoleAsync(role);
                return RedirectToAction("AllRoles", "Admin");
            }
            ModelState.AddModelError("", "Error creating role");
            return View();
        }
        [HttpGet]
        public ActionResult CreateUsers()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken] 
        public ActionResult CreateUsers(CreateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                _superadmin.CreateAsync(model);
                return RedirectToAction("AllUsers", "Admin");
            }
            ModelState.AddModelError("", "Error creating user");
            return View();
        }
        [HttpGet]
        public async Task<ActionResult> EditRole(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var role = await _superadmin.FindRoleByIdAsync(id);
            if(role == null)
            {
                return HttpNotFound();
            }
            return View(role);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRole(IdentityRole role)
        {
            if (ModelState.IsValid)
            {
                _superadmin.UpdateRoleAsync(role);
                ViewBag.Message = "Role Successfully Updated";
                return RedirectToAction("AllRoles", "Admin");
            }
            return View(role);
        }
        [HttpGet]
        public async Task<ActionResult> RoleDetails(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var role = await _superadmin.FindRoleByIdAsync(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            return View(role);
        }
        [HttpGet]
        public async Task<ActionResult> UserDetails(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await _superadmin.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }
        public async Task<ActionResult> EditUser(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await _superadmin.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser(CreateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                _superadmin.UpdateAsync(model);
                ViewBag.Message = "Role Successfully Updated";
                return RedirectToAction("AllUsers", "Admin");
            }
            return View(model);
        }
        public async Task<ActionResult> DeleteRole(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var role = await _superadmin.FindRoleByIdAsync(id);
            if (role != null)
            {
                var result = await _superadmin.DeleteRoleAsync(role);
                if (result.Succeeded)
                {
                    ViewBag.Message = "Role deleted successfully";
                    return RedirectToAction("AllRoles");
                }
                else
                {
                    ViewBag.Message = "Role cannot be deleted";
                }
            }
            else
            {
                ModelState.AddModelError("", "No role found");
                return View("AllRoles", _superadmin.GetAllRoles());
            }
            return View();
        }
       
       
       
    }
    
}