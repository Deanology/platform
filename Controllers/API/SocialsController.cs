﻿using Platform.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Platform.Controllers.API
{
    [System.Web.Http.Route("api/company/{id}/socials")]
    public class SocialsController : ApiController
    {
        private readonly ISocialsRepository _socials;
        public SocialsController(ISocialsRepository socials)
        {
            _socials = socials;
        }
        // GET: Socials
        public IHttpActionResult Index()
        {
            return Ok();
        }

    }
}