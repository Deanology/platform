﻿using AutoMapper;
using Platform.Models;
using Platform.Models.ViewModel;
using Platform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Controllers.API
{
    public class CompanyController : ApiController
    {
        private readonly ICompanyRepository _company;
        public CompanyController(ICompanyRepository company)
        {
            _company = company;
        }
        [Route("api/company")]
        [HttpGet]
        public IHttpActionResult GetAllCompanies()
        {
            try
            {
                var companyDb = _company.GetAllCompanies();
                var companies = Mapper.Map<IEnumerable<Company>, IEnumerable<CompanyViewModel>>(companyDb);
                return Ok(companies);
            }
            catch(Exception ex)
            {
                //TODO Add Logging
                return InternalServerError(ex);
            }
            
        }
        
        [Route("api/company")]
        [HttpPost]
        public IHttpActionResult AddCompany(CompanyViewModel model)
        {
            if (ModelState.IsValid)
            {
                _company.CreateCompany(Mapper.Map<CompanyViewModel, Company>(model));
                _company.SaveChanges();
                return Ok();
            }
            return BadRequest("Invalid data, Please recheck");
        }
        [Route("api/company/{id}")]
        [HttpGet]
        public IHttpActionResult GetCompanyById(int id)
        {

            var companyDb = _company.GetCompany(id);
            var company = Mapper.Map<CompanyViewModel>(companyDb);
            if (company == null)
            {
                return NotFound();
            }
            return Ok(company);
        }
        [Route("api/company/{id}")]
        [HttpPut]
        public IHttpActionResult UpdateCompany(int id, CompanyViewModel model)
        {
            var company = _company.GetCompany(id);
            if (company != null)
            {
                var companyDb = Mapper.Map(model, company);
                _company.UpdateCompany(companyDb);
                _company.SaveChanges();
                return Ok(Mapper.Map<CompanyViewModel>(companyDb));
            }
            else
            {
                return NotFound();
            }
        }

        [Route("api/company/{id}")]
        public IHttpActionResult DeleteCompany(int id)
        {
            _company.DeleteCompany(id);
            _company.SaveChanges();
            return Ok();
        }
    }
}