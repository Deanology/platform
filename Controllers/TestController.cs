﻿using Platform.Models;
using Platform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using File = Platform.Models.File;

namespace Platform.Controllers
{
    public class TestController : Controller
    {
        private readonly ITestRepository _test;
        public TestController(ITestRepository test)
        {
            _test = test;
        }
        // GET: Test
        public ActionResult Index()
        {
            var test = _test.GetAll();
            return View(test);
        }
        [HttpGet]
        public ActionResult AddTest()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddTest(Test test)
        {
            return View();
        }
        public ActionResult UploadSingleFile()
        {
            return View();
        }
        public ActionResult UploadMultipleFiles()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadSingleFile(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                string filename = Path.GetFileNameWithoutExtension(file.FileName);
                string extension = Path.GetExtension(file.FileName);
                filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                string filePath = "~/Images/" + filename;
                var path = Server.MapPath(filePath);
                file.SaveAs(path);
                // save to file database if need be
                var file1 = new File
                {
                    FileName = filename,
                    FilePath = filePath
                };
                //insert file
                _test.Save();

            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadMultipleFiles(IEnumerable<HttpPostedFileBase> files)
        {
            foreach (var file in files)
            {
                if (file != null && file.ContentLength > 0)
                {
                    string filename = Path.GetFileNameWithoutExtension(file.FileName);
                    string extension = Path.GetExtension(file.FileName);
                    filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                    string filePath = "~/Images/" + filename;
                    var path = Server.MapPath(filePath);
                    file.SaveAs(path);
                    // save to file database if need be
                    var file1 = new File
                    {
                        FileName = filename,
                        FilePath = filePath
                    };
                    //save to db
                   
                }
                _test.Save();
            }
            return View();
        }
    }
}