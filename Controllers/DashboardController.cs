﻿using AutoMapper;
using Microsoft.AspNet.Identity.EntityFramework;
using Platform.Models;
using Platform.Models.ViewModel;
using Platform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Platform.Controllers
{
    [Authorize (Roles = "Admin")]
    public class DashboardController : Controller
    {
        // GET: Dashboard
        private readonly IDashboardRepository _dashboard;
        private readonly IRoleRepository _role;
        public DashboardController(IDashboardRepository dashboard, IRoleRepository role)
        {
            _dashboard = dashboard;
            _role = role;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult RolePermissions()
        {
            return View();
        }
        public ActionResult AllCategories()
        {
            var categoriesDb = _dashboard.GetAll();
            var categories = Mapper.Map<List<Category>, List<CategoryViewModel>>((List<Category>)categoriesDb);
            return View(categories);
        }
        public ActionResult GetAllRoles()
        {
            var roles = _role.GetAllRoles();
            return View(roles);
        }
        public ActionResult CategoryDetails(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var categoryDb = _dashboard.GetById(id);
            var category = Mapper.Map<Category, CategoryViewModel>(categoryDb);
            return View(category);
        }
        public ActionResult CreateCategory()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCategory(CategoryViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var category = Mapper.Map<CategoryViewModel, Category>(model);
                    category.CreatedAt = DateTime.Now;
                    category.UpdatedAt = DateTime.Now;
                    _dashboard.Insert(category);
                    _dashboard.Save();
                    TempData["category"] = "Category Successfully Added";
                    ModelState.Clear();
                    return RedirectToAction("CreateCategory", "Dashboard");
                }
            }
            catch (RetryLimitExceededException)
            {
                //Log the error 
                ModelState.AddModelError("", "Unable to add category. Try again, and if the problem persists see your system administrator.");
            }
            return View();
        }
        public ActionResult EditCategory(int? id)
        {
            var categoryDb = _dashboard.GetById(id);
            var category = Mapper.Map<Category, CategoryViewModel>(categoryDb);
            return View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCategory(CategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var categoryDb = _dashboard.GetById(model.Id);
                    var category = Mapper.Map(model, categoryDb);
                    category.UpdatedAt = DateTime.Now;
                    _dashboard.Update(category);
                    _dashboard.Save();
                    TempData["category"] = "Category Successfully Updated";
                    return RedirectToAction("EditCategory", "Dashboard");
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }


            return View("EditCategory");
        }
        public ActionResult DeleteCategory(int? id)
        {
            if (id != null)
            {
                _dashboard.Delete(id.Value);
                _dashboard.Save();
                TempData["category"] = "Category Successfully Deleted";
                return RedirectToAction("AllCategories", "Dashboard");
            }
            return HttpNotFound();
        }
        public ActionResult CreateRole()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateRole(CreateRoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _role.CreateRoleAsync(model.Name);
                if (result.Succeeded)
                {
                    TempData["role"] = "Role Successfully Created";
                    ModelState.Clear();
                    return RedirectToAction("CreateRole", "Dashboard");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }
            return View(model.Name);
        }
        [HttpPost]
        public async Task<ActionResult> DeleteRole(string id)
        {
            var result = await _dashboard.DeleteRoleAsync(id);
            if (result.Succeeded)
            {
                TempData["success"] = "Deleted Successfully";
                return RedirectToAction("GetAllRoles", "Dashboard");
            }
            TempData["failure"] = "Roles cannot be deleted";
            return View("GetAllRoles", "Dashboard");
        }
        [HttpGet]
        public async Task<ActionResult> EditRole(string id)
        {
            var role = await _role.FindRoleByIdAsync(id);
            return View(role);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateRole(string rolename)
        {
            if (ModelState.IsValid)
            {
                var result = await _role.UpdateRoleAsync(rolename);
                if (result.Succeeded)
                {
                    TempData["roleUpdate"] = "Role Successfully Updated";
                    return RedirectToAction("GetAllRoles", "Dashboard");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }
            return View("EditRole");
        }
    }
}