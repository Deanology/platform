﻿using AutoMapper;
using Platform.Models;
using Platform.Models.ViewModel;
using Platform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Platform.Controllers
{
    public class JobsController : Controller
    {
        private readonly IDashboardRepository _dashboard;
        private readonly IJobRepository _job;
        public JobsController(IDashboardRepository dashboard, IJobRepository job)
        {
            _dashboard = dashboard;
            _job = job;
        }
        // GET: Jobs
        public ActionResult Index()
        {
            var jobDb = _job.Jobs();
            return View(jobDb);
        }
        public ActionResult JobDetails(int id)
        {
            var job = Mapper.Map<Job, JobViewModel>(_job.GetById(id));
            return View(job);
        }
        public ActionResult CreateJob()
        {
            var model = new JobViewModel
            {
                Categories = Mapper.Map<IEnumerable<Category>, IEnumerable<CategoryViewModel>>(_dashboard.GetAll())
            };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateJob(JobViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _job.CreateJob(model);
                if (result)
                {
                    TempData["jobMsg"] = "Job Successfully Added";
                    return RedirectToAction("CreateJob");
                }
                else
                {
                    TempData["jobMsg"] = "Job cannot be added";
                    return RedirectToAction("CreateJob");
                }
                
            }
            else
            {
                ModelState.AddModelError(string.Empty, "An Internal Error Occured");
                model.Categories = Mapper.Map<IEnumerable<Category>, IEnumerable<CategoryViewModel>>(_dashboard.GetAll());
                return View("CreateJob",model);
            }
        }
    }
}