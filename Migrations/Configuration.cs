namespace Platform.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Platform.Models;
    using Platform.Services.Helper_Classes;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Platform.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Platform.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            /*Create Some Roles*/
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var role = new CreateRoleService(context);
            role.Create("Admin");
            role.Create("Customers");
            role.Create("Superadmin");
            
            var user = new ApplicationUser
            {
                UserName = "superadmin@gmail.com",
                Email = "superadmin@gmail.com",
                EmailConfirmed = true,
            };
            var result = manager.Create(user, "QWerty55!");
            if (result.Succeeded)
            {
                manager.AddToRole(user.Id, "Superadmin");
            }

            var admin = new ApplicationUser
            {
                UserName = "admin@gmail.com",
                Email = "admin@gmail.com",
                EmailConfirmed = true,
            };
            var result2 = manager.Create(admin, "ZXcvbn55!");
            if (result2.Succeeded)
            {
                manager.AddToRole(admin.Id, "Admin");
            }
        }
    }
}
