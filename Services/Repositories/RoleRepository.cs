﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Platform.Models;
using Platform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using static System.Web.HttpContext;

namespace Platform.Services.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationRoleManager _roleManager;

        public RoleRepository()
        {
            //uses the system.web import
            _context = Current.GetOwinContext().Get<ApplicationDbContext>();
            _userManager = Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            _roleManager = Current.GetOwinContext().Get<ApplicationRoleManager>();
        }
        public IEnumerable<IdentityRole> GetAllRoles()
        {
            return _roleManager.Roles.ToList();
        }
        public async Task<IdentityResult> CreateRoleAsync(string rolename)
        {
            if (!_roleManager.RoleExists(rolename))
            {
                var result = await _roleManager.CreateAsync(new IdentityRole(rolename));
                return result;
            }
            return IdentityResult.Failed();
        }
        public async Task<IdentityResult> UpdateRoleAsync(string id)
        {
            var role = await FindRoleByIdAsync(id);
            return await _roleManager.UpdateAsync(role);
        }
        public async Task<IdentityRole> FindRoleByIdAsync(string id)
        {
            return await _roleManager.FindByIdAsync(id);
        }

        public async Task<IdentityRole> FindRoleByNameAsync(string rolename)
        {
            return await _roleManager.FindByNameAsync(rolename);
        }
    }
}