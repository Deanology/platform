﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Platform.Models;
using Platform.Models.ViewModel;
using Platform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using static System.Web.HttpContext;

namespace Platform.Services.Repositories
{
    public class DashboardRepository : GenericRepository<Category>,IDashboardRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationRoleManager _roleManager;
        
        public DashboardRepository()
        {
            //uses the system.web import
            _context = Current.GetOwinContext().Get<ApplicationDbContext>();
            _userManager = Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            _roleManager = Current.GetOwinContext().Get<ApplicationRoleManager>();
        }


        public Task<IdentityResult> AddToRoleAsync(string userId, string roleId)
        {
            throw new NotImplementedException();
        }

        public async Task<IdentityResult> DeleteRoleAsync(string roleid)
        {
            var role = await _roleManager.FindByIdAsync(roleid);
            return await _roleManager.DeleteAsync(role);
        }

       

        public async Task<IdentityRole> FindRoleByIdAsync(string id)
        {
            return await _roleManager.FindByIdAsync(id);
        }

        public Task<IdentityRole> FindRoleByNameAsync(string rolename)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> RemoveFromRoleAsync(string userId, string roleId)
        {
            throw new NotImplementedException();
        }

        

    }
}