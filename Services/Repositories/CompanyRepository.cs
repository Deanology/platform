﻿using Microsoft.AspNet.Identity.Owin;
using Platform.Models;
using Platform.Models.ViewModel;
using Platform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using static System.Web.HttpContext;

namespace Platform.Services.Repositories
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly ApplicationDbContext _context;
        public CompanyRepository(ApplicationDbContext context)
        {
            _context = Current.GetOwinContext().Get<ApplicationDbContext>();
        }

        public void CreateCompany(Company company)
        {
            _context.Companies.Add(company);
        }
        public void UpdateCompany(Company model)
        {
            _context.Entry(model).State = EntityState.Modified;
        }
        public void DeleteCompany(int id)
        {
            var company = GetCompany(id);
            _context.Companies.Remove(company);
        }
        public IEnumerable<Company> GetAllCompanies()
        {
            return _context.Companies
                .Include(c => c.Location)
                .Include(s => s.Socials).ToList();
        }

        public Company GetCompany(int id)
        {
            return _context.Companies.FirstOrDefault(p => p.Id == id);
        }

        public void SaveChanges()
        {
           _context.SaveChanges();
        }
    }
}