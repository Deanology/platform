﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Platform.Models;
using Platform.Models.ViewModel;
using Platform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Platform.Services.Repositories
{
    public class AdminRepository : IAdminRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ApplicationDbContext _context;

        public AdminRepository(ApplicationDbContext context)
        {
            _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            _context = context;
        }


        public async Task AddToRoleAsync(ApplicationUser user, string roleName)
        {
            var role = await _roleManager.FindByNameAsync(roleName);
            user.Roles.Add(new IdentityUserRole() { UserId = user.Id, RoleId = role.Id});
            await _userManager.UpdateAsync(user);
        }
        public async Task CreateRoleAsync(IdentityRole role)
        {
            await _roleManager.CreateAsync(role);
        }
        public async Task CreateAsync(CreateUserViewModel model)
        {
            var user = Mapper.Map<CreateUserViewModel, ApplicationUser>(model);
            await _userManager.CreateAsync(user, model.Password);
        }

        public async Task DeleteAsync(ApplicationUser user)
        {
            await _userManager.DeleteAsync(user);
        }

        public async Task<ApplicationUser> FindByEmailAsync(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<CreateUserViewModel> FindByIdAsync(string userId)
        {
            var userDb =  await _userManager.FindByIdAsync(userId);
            var user = Mapper.Map<ApplicationUser, CreateUserViewModel>(userDb);
            return user;
        }
        public async Task<ApplicationUser> FindByIdAsyncDb(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            return user;
        }

        public async Task<ApplicationUser> FindByNameAsync(string username)
        {
            return await _userManager.FindByNameAsync(username);
        }

        public IEnumerable<ApplicationUser> GetAllUsers()
        {
            return _userManager.Users.ToList();
        }
        
        public IEnumerable<IdentityRole> GetAllRoles()
        {
            return _context.Roles.ToList();
        }

        public  Task<string> GetPasswordHashAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<string>> GetRolesAsync(ApplicationUser user)
        {
            var roles = new List<string>();
            var userRoles = await _userManager.GetRolesAsync(user.Id);
            foreach (var item in userRoles)
            {
                roles.Add(item);
            }
            return roles;
        }

        public Task<string> HasPasswordHashAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> IsInRoleAsync(ApplicationUser user, string roleName)
        {
            var role = await _roleManager.FindByNameAsync(roleName);
            foreach (var item in user.Roles)
            {
                if (item.RoleId == role.Id)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task RemoveFromRoleAsync(ApplicationUser user, string roleName)
        {
            /*var role = await _roleManager.FindByNameAsync(roleName);
            var roleToRemove = user.Roles.Single(role.Users);
            user.Roles.Remove(roleToRemove);
            await _userManager.UpdateAsync(user);*/
            await _userManager.RemoveFromRoleAsync(user.Id, roleName);
            await _userManager.UpdateAsync(user);
        }

        public async Task SetPasswordHashAsync(ApplicationUser user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            await _userManager.UpdateAsync(user);
        }
        
        public async Task UpdateAsync(CreateUserViewModel model)
        {
            var userDb = await FindByIdAsyncDb(model.User.Id);
            var user = Mapper.Map(model, userDb);
            await _userManager.UpdateAsync(user);
        }

        public async Task<IdentityResult> DeleteRoleAsync(IdentityRole role)
        {
            return await _roleManager.DeleteAsync(role);
        }

        public async Task<IdentityRole> FindRoleByIdAsync(string id)
        {
            return await _roleManager.FindByIdAsync(id);
        }

        public async Task<IdentityRole> FindRoleByNameAsync(string rolename)
        {
            return await _roleManager.FindByNameAsync(rolename);
        }

        public async Task UpdateRoleAsync(IdentityRole role)
        {
            await _roleManager.UpdateAsync(role);
        }

        public Task<bool> RoleExistsAsync(string rolename)
        {
            return _roleManager.RoleExistsAsync(rolename);
        }
    }
}