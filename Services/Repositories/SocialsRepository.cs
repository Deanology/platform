﻿using Microsoft.AspNet.Identity.Owin;
using Platform.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static System.Web.HttpContext;

namespace Platform.Services.Repositories
{
    public class SocialsRepository : GenericRepository<Socials>, ISocialsRepository
    {
        private readonly ApplicationDbContext _context;
        public SocialsRepository()
        {
            _context = Current.GetOwinContext().Get<ApplicationDbContext>();
        }
        
    }
}