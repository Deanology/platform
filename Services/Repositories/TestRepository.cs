﻿using Platform.Models;
using Platform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Services.Repositories
{
    public class TestRepository : GenericRepository<Test>, ITestRepository
    {
    }
}