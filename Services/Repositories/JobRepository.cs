﻿using AutoMapper;
using Microsoft.AspNet.Identity.Owin;
using Platform.Models;
using Platform.Models.ViewModel;
using Platform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using static System.Web.HttpContext;

namespace Platform.Services.Repositories
{
    public class JobRepository : GenericRepository<Job>, IJobRepository
    {
        private readonly ApplicationDbContext _context;
        public JobRepository()
        {
            _context = Current.GetOwinContext().Get<ApplicationDbContext>();
        }
        public bool CreateJob(JobViewModel model)
        {
            var job = Mapper.Map<JobViewModel, Job>(model);
            job.CreatedAt = DateTime.Now;
            job.UpdatedAt = DateTime.Now;
            _context.Jobs.Add(job);
            _context.SaveChanges();
            return true;
        }
        public IEnumerable<Job> Jobs()
        {
            var jobs = _context.Jobs.Include(c => c.Category).ToList();
            return jobs;
        }
    }
}