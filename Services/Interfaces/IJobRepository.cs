﻿using Platform.Models;
using Platform.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Services.Interfaces
{
    public interface IJobRepository : IGenericRepository<Job>
    {
        bool CreateJob(JobViewModel model);
        IEnumerable<Job> Jobs();
    }
}
