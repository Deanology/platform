﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Platform.Models;
using Platform.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Services.Interfaces
{
    public interface IAdminRepository
    {
        /// <summary>
        /// Application User
        /// </summary>
        /// <returns></returns>
        IEnumerable<ApplicationUser> GetAllUsers();
        Task CreateAsync(CreateUserViewModel model);
        Task DeleteAsync(ApplicationUser user);
        Task<CreateUserViewModel> FindByIdAsync(string userId);
        Task<ApplicationUser> FindByNameAsync(string username);
        Task<ApplicationUser> FindByEmailAsync(string email);
        Task UpdateAsync(CreateUserViewModel model);
        Task<string> GetPasswordHashAsync(ApplicationUser user);
        Task<string> HasPasswordHashAsync(ApplicationUser user);
        Task SetPasswordHashAsync(ApplicationUser user, string passwordHash);
        Task AddToRoleAsync(ApplicationUser user, string roleName);
        Task<IEnumerable<string>> GetRolesAsync(ApplicationUser user);
        Task<bool> IsInRoleAsync(ApplicationUser user, string roleName);

        Task RemoveFromRoleAsync(ApplicationUser user, string roleName);
        /// <summary>
        /// Role Methods
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        /// 
        IEnumerable<IdentityRole> GetAllRoles();
        Task CreateRoleAsync(IdentityRole role);    
        Task<IdentityResult> DeleteRoleAsync(IdentityRole role);   
        Task<IdentityRole> FindRoleByIdAsync(string id);        
        Task<IdentityRole> FindRoleByNameAsync(string rolename);
        Task UpdateRoleAsync(IdentityRole role);
        Task<bool> RoleExistsAsync(string rolename);

       
        
    }
}
