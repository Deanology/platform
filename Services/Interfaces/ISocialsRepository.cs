﻿using Platform.Models;
using Platform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Services.Repositories
{
    public interface ISocialsRepository : IGenericRepository<Socials>
    {
    }
}
