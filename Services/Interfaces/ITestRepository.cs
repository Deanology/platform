﻿using Platform.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Services.Interfaces
{
    public interface ITestRepository : IGenericRepository<Test>
    {

    }
}
