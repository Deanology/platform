﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Services.Interfaces
{
    public interface IRoleRepository
    {
        Task<IdentityRole> FindRoleByIdAsync(string id);
        Task<IdentityRole> FindRoleByNameAsync(string rolename);
        IEnumerable<IdentityRole> GetAllRoles();
        Task<IdentityResult> CreateRoleAsync(string rolename);
        Task<IdentityResult> UpdateRoleAsync(string rolename);
    }
}
