﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Platform.Models;
using Platform.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Services.Interfaces
{
    public interface IDashboardRepository : IGenericRepository<Category>
    {


        /// <summary>
        /// Role Methods
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        /// 
        Task<IdentityResult> DeleteRoleAsync(string roleid);
        Task<IdentityRole> FindRoleByIdAsync(string id);
        Task<IdentityRole> FindRoleByNameAsync(string rolename);
        Task<IdentityResult> AddToRoleAsync(string userId, string roleId);

        Task<IdentityResult> RemoveFromRoleAsync(string userId, string roleId);
    }
}
