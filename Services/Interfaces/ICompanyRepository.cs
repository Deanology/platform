﻿using Platform.Models;
using Platform.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Services.Interfaces
{
    public interface ICompanyRepository
    {
        IEnumerable<Company> GetAllCompanies();
        Company GetCompany(int id);
        void CreateCompany(Company company);
        void UpdateCompany(Company model);
        void DeleteCompany(int id);
        void SaveChanges();
    }
}
