﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Models
{
    public class Company
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string CompanyWebsiteURL { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyDescription { get; set; }
        public int LocationId { get; set; }
        public Location Location{ get; set; }
        public int SocialsId { get; set; }
        public Socials Socials { get; set; }
    }
}