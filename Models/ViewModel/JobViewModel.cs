﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Platform.Models.ViewModel
{
    public class JobViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Job title cannot be blank")]
        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }
        [Required(ErrorMessage = "Enter Price!")]
        [Range(1, int.MaxValue, ErrorMessage = "Invalid Input")]
        public int Vacancies { get; set; }
        [Required(ErrorMessage = "Category Select a Category!")]
        [DisplayName("Category")]
        public int CategoryId { get; set; }
        public IEnumerable<CategoryViewModel> Categories { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Select a correct Job Type")]
        [Display(Name = "Job Type")]
        public JobTypes JobTypesId { get; set; }
        [DisplayName("Expiry Date")]
        [DataType(dataType: DataType.DateTime)]
        public DateTime? ExpireDate { get; set; }
        [Required(ErrorMessage = "Give A Description!")]
        [DisplayName("Job Description")]
        [DataType(DataType.MultilineText)]
        public string JobDescription { get; set; }
        [Required(ErrorMessage = "Give A Skills!")]
        [DisplayName("Required Knowledge, Skills, and Abilities")]
        [DataType(DataType.MultilineText)]
        public string Skills { get; set; }
        [Required(ErrorMessage = "Give A Education!")]
        [DisplayName("Education Qualification")]
        [DataType(DataType.MultilineText)] 
        public string Education { get; set; }
        [Required]
        [Range(20000.99, double.MaxValue)]
        public double MinPay { get; set; }

        public double MaxPay { get; set; }
        [Required]
        public int MinYearExperience { get; set; }
        [Range(1, 50)]
        public int MaxYearExperience { get; set; }
    }
}