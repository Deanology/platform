﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Platform.Models.ViewModel
{
    public class CreateRoleViewModel
    {
        public string Id { get; set; }
        [Display(Name = "Role Name")]
        [Required(ErrorMessage = "Please enter the role name")]
        [MaxLength(20)]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        public string Name { get; set; }
    }
}