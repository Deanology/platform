﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Platform.Models.ViewModel
{
    public class TestViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter a Test")]
        [Display(Name = "Test Name")]
        public string TestName { get; set; }
        [DataType(DataType.Upload)]
        [Display(Name = "Upload Image")]
        [Required(ErrorMessage = "Please choose file to upload. ")]
        public string FilePath { get; set; }
        public HttpPostedFileBase File { get; set; }
        
    }
}