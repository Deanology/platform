﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Models.ViewModel
{
    public class SocialViewModel
    {
        public int Id { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
        public string Behance { get; set; }
        public string LinkedIn { get; set; }
    }
}