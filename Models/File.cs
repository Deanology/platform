﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Models
{
    public class File
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}