﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}