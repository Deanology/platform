﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Models
{
    public class Job
    {
        public int Id { get; set; }
        public string JobTitle { get; set; }
        public int Vacancies { get; set; }
        public double MinPay { get; set; }
        public double MaxPay { get; set; }
        public int MinYearExperience { get; set; }
        public int MaxYearExperience { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public JobTypes JobTypeID { get; set; }
        public DateTime? ExpireDate { get; set; }
        public string JobDescription { get; set; }
        public string Skills { get; set; }
        public string Education { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}