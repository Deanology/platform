﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Models.DataLayer
{
    public class CompanyDTO
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string CompanyWebsiteURL { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyDescription { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string LinkedIn { get; set; }
        public string Instagram { get; set; }
        public string Behance { get; set; }
        public string GooglePlus { get; set; }
    }
}