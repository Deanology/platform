﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Platform.Models
{
    public enum JobTypes
    {
        [Display(Name = "Part Time")]
        PartTime = 1,
        [Display(Name = "Full Time")]
        FullTime = 2,
        [Display(Name = "Internship")]
        Internship = 3,
        [Display(Name = "Contract")]
        Contract = 4,
        [Display(Name = "Remote")]
        Remote = 5
    }
}