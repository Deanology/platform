﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Ninject;
using Platform.Models;
using Platform.Services.Interfaces;
using Platform.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Platform.Infrastructure
{
    public class DependencyResolver : IDependencyResolver
    {
        private readonly IKernel _kernel;

        public DependencyResolver(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        private void AddBindings()
        {
            _kernel.Bind<ApplicationDbContext>().ToSelf();
            _kernel.Bind<IAdminRepository>().To<AdminRepository>();
            _kernel.Bind<IDashboardRepository>().To<DashboardRepository>();
            _kernel.Bind<IRoleRepository>().To<RoleRepository>();
            _kernel.Bind<IJobRepository>().To<JobRepository>();
            _kernel.Bind<ICompanyRepository>().To<CompanyRepository>();
            _kernel.Bind<ISocialsRepository>().To<SocialsRepository>();
            _kernel.Bind<ITestRepository>().To<TestRepository>();
            _kernel.Bind<IUserStore<ApplicationUser>>().To<UserStore<ApplicationUser>>();
            _kernel.Bind<UserManager<ApplicationUser>>().ToSelf();
            _kernel.Bind<IRoleStore<IdentityRole, string>>().To<RoleStore<IdentityRole, string, IdentityUserRole>>();

        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }
    }
}