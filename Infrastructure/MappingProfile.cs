﻿using AutoMapper;
using Platform.Models;
using Platform.Models.DataLayer;
using Platform.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Infrastructure
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<ApplicationUser, CreateUserViewModel>().ReverseMap();
            Mapper.CreateMap<Category, CategoryViewModel>().ForSourceMember(x => x.UpdatedAt, y => y.Ignore())
                .ForSourceMember(a => a.CreatedAt, b => b.Ignore()).ReverseMap();
            Mapper.CreateMap<User, RegisterViewModel>().ReverseMap().ForSourceMember(x => x.ConfirmPassword, y => y.Ignore());
            Mapper.CreateMap<Job, JobViewModel>().ReverseMap();
            Mapper.CreateMap<Company, CompanyViewModel>().ReverseMap();
            Mapper.CreateMap<Socials, SocialViewModel>().ReverseMap();

        }
    }
}