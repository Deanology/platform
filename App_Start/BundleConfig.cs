﻿using System.Web;
using System.Web.Optimization;

namespace Platform
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/Static/assets").Include(
                       "~/Static/assets/js/jquery.min.js",
                       "~/Static/assets/js/popper.min.js",
                       "~/Static/assets/js/bootstrap.min.js",
                       "~/Static/assets/js/jquery.meanmenu.js",
                       "~/Static/assets/js/jquery.nice-select.min.js",
                       "~/Static/assets/js/jquery.mixitup.min.js",
                       "~/Static/assets/js/waypoints.min.js",
                       "~/Static/assets/js/jquery.counterup.min.js",
                       "~/Static/assets/js/owl.carousel.min.js",
                       "~/Static/assets/js/jquery.ajaxchimp.min.js",
                        "~/Static/assets/js/form-validator.min.js",
                         "~/Static/assets/js/contact-form-script.js",
                          "~/Static/assets/js/wow.min.js",
                          "~/Static/assets/js/custom.js"
                       ));
            bundles.Add(new ScriptBundle("~/Static/admin").Include(
                       "~/Static/admin/plugins/jquery/jquery.min.js",
                        "~/Static/admin/plugins/jquery-ui/jquery-ui.min.js",
                        "~/Static/admin/plugins/bootstrap/js/bootstrap.bundle.min.js",
                        "~/Static/admin/dist/js/adminlte.js",
                        "~/Static/admin/plugins/chart.js/Chart.min.js",
                        "~/Static/admin/dist/js/demo.js",
                        "~/Static/admin/dist/js/pages/dashboard3.js",
                        "~/Static/admin/plugins/sparklines/sparkline.js",
                        "~/Static/admin/plugins/jqvmap/jquery.vmap.min.js",
                        "~/Static/admin/plugins/jqvmap/maps/jquery.vmap.usa.js",
                        "~/Static/admin/plugins/jquery-knob/jquery.knob.min.js",
                        "~/Static/admin/plugins/moment/moment.min.js",
                        "~/Static/admin/plugins/daterangepicker/daterangepicker.js",
                        "~/Static/admin/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js",
                        "~/Static/admin/plugins/summernote/summernote-bs4.min.js",
                        "~/Static/admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js",
                        "~/Static/admin/dist/js/pages/dashboard.js"
                ));
            bundles.Add(new ScriptBundle("~/Static/admin2").Include(
                       "~/Static/admin2/vendors/jquery/dist/jquery.min.js",
                        "~/Static/admin2/vendors/popper.js/dist/umd/popper.min.js",
                        "~/Static/admin2/vendors/bootstrap/dist/js/bootstrap.min.js",
                        "~/Static/admin2/assets/js/main.js",
                        "~/Static/admin2/vendors/chart.js/dist/Chart.bundle.min.js",
                        "~/Static/admin2/assets/js/dashboard.js",
                        "~/Static/admin2/assets/js/widgets.js",
                        "~/Static/admin2/vendors/jqvmap/dist/jquery.vmap.min.js",
                        "~/Static/admin2/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js",
                        "~/Static/admin2/vendors/datatables.net/js/jquery.dataTables.min.js",
                        "~/Static/admin2/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js",
                        "~/Static/admin2/vendors/datatables.net-buttons/js/dataTables.buttons.min.js",
                        "~/Static/admin2/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js",
                        "~/Static/admin2/vendors/jszip/dist/jszip.min.js",
                        "~/Static/admin2/vendors/pdfmake/build/pdfmake.min.js",
                        "~/Static/admin2/vendors/pdfmake/build/vfs_fonts.js",
                        "~/Static/admin2/vendors/datatables.net-buttons/js/buttons.html5.min.js",
                        "~/Static/admin2/vendors/datatables.net-buttons/js/buttons.print.min.js",
                        "~/Static/admin2/vendors/datatables.net-buttons/js/buttons.colVis.min.js",
                        "~/Static/admin2/assets/js/init-scripts/data-table/datatables-init.js"
                ));
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
            bundles.Add(new StyleBundle("~/Static/assets").Include(
                       "~/Static/assets/css/bootstrap.min.css",
                        "~/Static/assets/css/meanmenu.css",
                         "~/Static/assets/css/icofont.min.css",
                          "~/Static/assets/css/nice-select.css",
                           "~/Static/assets/css/owl.carousel.min.css",
                            "~/Static/assets/css/owl.theme.default.min.css",
                             "~/Static/assets/css/animate.css",
                              "~/Static/assets/fonts/flaticon.css",
                               "~/Static/assets/css/style.css",
                                "~/Static/assets/css/responsive.css",
                                 "~/Static/assets/css/rtl.css"
                ));
            bundles.Add(new StyleBundle("~/Static/admin").Include(
                    "~/Static/admin/plugins/fontawesome-free/css/all.min.css",
                    "~/Static/admin/dist/css/adminlte.min.css",
                    "~/Static/admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css",
                    "~/Static/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css",
                    "~/Static/admin/plugins/jqvmap/jqvmap.min.css",
                    "~/Static/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css",
                    "~/Static/admin/plugins/daterangepicker/daterangepicker.css",
                    "~/Static/admin/plugins/summernote/summernote-bs4.min.css"
                ));
            bundles.Add(new StyleBundle("~/Static/admin2").Include(
                    "~/Static/admin2/vendors/bootstrap/dist/css/bootstrap.min.css",
                    "~/Static/admin2/vendors/font-awesome/css/font-awesome.min.css",
                    "~/Static/admin2/vendors/themify-icons/css/themify-icons.css",
                    "~/Static/admin2/vendors/flag-icon-css/css/flag-icon.min.css",
                    "~/Static/admin2/vendors/selectFX/css/cs-skin-elastic.css",
                    "~/Static/admin2/vendors/jqvmap/dist/jqvmap.min.css",
                    "~/Static/admin2/assets/css/style.css"
                ));
        }
    }
}
